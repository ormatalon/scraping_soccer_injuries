
#######################################################################################
#######################################################################################
#####                                                                             #####
#####       Scraping injuries from few leagues the soccerway website              #####
#####                                                                             #####
#######################################################################################
#######################################################################################

### imports and constants

import requests
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np

BASE = "https://us.soccerway.com/"
COMP_TAB = "/competition/"
PLAYERS_TAB = "/squad/"
N_LEAGUES = 5
START_REF = 'href="'
END_REF = '">'

### Start with loading the first page

def get_html_from_url(url = BASE, tab = COMP_TAB):
    req = requests.get(url+tab)
    soup = BeautifulSoup(req.content, 'html.parser')
    return soup

"""def find_nth(string_source, substring, n):
    """
    """find the n instance of a substring
    :param string: the string in which to search
    :param substring: the sub string to search in the string 
    :param n: the nth appearance we are looking for
    :return: """
    """
    if (n == 1):
        return string_source.find(substring)
    else:
        return string_source.find(substring, find_nth(string_source, substring, n - 1) + 1)"""

def find_within_league(leagues_input = leagues_list, league_rank = 0):
    """

    :param leagues_input:
    :param league_rank:
    :return:
    """
    league_url = leagues_input[league_rank].a["href"]
    league_html = get_html_from_url(url = BASE, tab = league_url)

    teams_in_league = league_html.find(class_ = 'table').find_all('td',class_="text team large-link")
        for team_rank in range(len(teams_in_league)):
            players = find_within_team(teams_in_league, team_rank, league = leagues_input[league_rank].string)
        return players


def find_within_team(teams_in_league, team_rank, league):
    team_url = teams_in_league[team_rank].a["href"]
    squad_html = get_html_from_url(BASE + team_url,tab=PLAYERS_TAB)
    squad_table = squad_html.find('table', class_="table squad sortable")
    players_class = squad_table.find_all('td', class_="name large-link")

    team_name = teams_in_league[team_rank].string
    team_players = {}
    for player in players_class:
        team_players[player.string]['url'] = player.a["href"]
        player_html = get_html_from_url(BASE,team_players[player.string]['url'])

        sideline_table = player_html.find('table', class_="sidelined table")
        team_players[player.string]['injuries'] = {}
        for i,injury in enumerate(sideline_table.find_all('tr', class_="odd")):
            team_players[player.string]['injuries'][i] = {}
            team_players[player.string]['injuries'][i]['type'] = injury.td["title"]
            team_players[player.string]['injuries'][i]['start'] = injury.find('td', class_="startdate").string
            team_players[player.string]['injuries'][i]['end'] = injury.find('td', class_="enddate").string

###
### The program itself
###

comp_html = get_html_from_url(url = BASE,tab = COMP_TAB)

leagues_list = comp_html.find_all("li", class_=["odd","even"])

for league_rank in range(N_LEAGUES):
    find_within_league(leagues_input = leagues_list, league_rank = N_LEAGUES)



    ind_start = find_nth(str(leagues_list[0]), 'href=', n)
    ind_end = str(leagues_list[0])[ind_tmp:].find('">')
    league_url = str(leagues_list[0])[(ind_tmp+6):][:(ind_end-6)]





# nonsense

soccerway_soup = get_html_from_url(BASE,'')
a_t_l = soccerway_soup.find_all('a',attrs={'class':'t_l','href':"/competitions/"})

a_t_l.__getitem__(0)

# As soon as I get the "/competitions/" I simply attach it to the original path
ind_start = str(leagues_list[0]).find('href=')
ind_end = str(leagues_list[0])[ind_tmp:].find('">')
league_url = str(leagues_list[0])[(ind_tmp+6):][:(ind_end-6)]
BASE + next_url

input_injuries_table = pd.read_json(r'C:\Users\user\Desktop\ITC_bootcamp\scraping_soccer_injuries\Team_Players.json')

# We create a unique id for each player, team and league that will later use us to find information from other tables
PLAYER_PATH = r'C:\Users\user\Desktop\ITC_bootcamp\scraping_soccer_injuries\Team_Players.json'

def extract_league_info(input_injuries_table):
    leagues_info = pd.DataFrame()
    leagues_info['name'] = input_injuries_table.columns
    leagues_info['id'] = list(leagues_info.index)
    return leagues_info


def extract_team_info(input_injuries_table):
    teams_info = pd.DataFrame()
    teams_info['name'] = input_injuries_table.index
    teams_info['id'] = list(teams_info.index)
    return teams_info


def extract_player_info(input_injuries_table):
    player_info = pd.DataFrame()
    for league_ind,league in enumerate(input_injuries_table.keys()):
        teams_per_league = input_injuries_table[league].keys()[input_injuries_table[league].notnull()]
        print("reading {} teams".format(league))
        for team_ind, team in enumerate(teams_per_league):
            players_in_team = input_injuries_table[league][team]
            for player_ind, player in enumerate(players_in_team):
                players_in_team[player]['url']
                if (player_ind == 0 and team_ind == 0 and league_ind == 0):
                    player_info = pd.DataFrame(pd.Series(players_in_team[player]['Info'])).T
                else:
                    player_info = player_info.append(pd.Series(players_in_team[player]['Info']),ignore_index=True)



    player_info['id'] = list(player_info.index)
    return player_info


def extract_player_info(input_injuries_table):
    columns_list = list(pd.DataFrame.from_dict(input_injuries_table["England"]["Liverpool"]).iloc[1,0]['1'].keys())
    columns_list.extend(["player","team","league"])
    injuries_table = pd.DataFrame(columns = columns_list)
    for league in input_injuries_table.keys():
        teams_per_league = input_injuries_table[league].keys()[input_injuries_table[league].notnull()]
        print("reading {} teams".format(league))
        for team_ind, team in enumerate(teams_per_league):
            team_df = pd.DataFrame.from_dict(input_injuries_table[league][team])
            for player_num in range(team_df.shape[1]):
                #id.extend(list(str(player_num) + str(i) for i in range(pd.DataFrame(data=player_injuries).shape[1])))
                player_injuries = team_df.iloc[1,player_num]
                injuries_table = injuries_table.append(pd.DataFrame(data=player_injuries).T)


def main():
    input_injuries_table = pd.read_json(PLAYER_PATH)
    leagues = extract_league_info(input_injuries_table)
    teams = extract_team_info(input_injuries_table)
    players = extract_player_info(input_injuries_table)
    injuries = extract_player_info(input_injuries_table)

