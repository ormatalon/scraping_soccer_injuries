"""
This module scrapes data regarding soccer players' injuries from the website
Soccerway.com . It allows saving 3 JSON files, each contains the following:
1. Links for all top N soccer leagues (most popular ones).
2. Links for all teams within each of these leagues.
3. Links, personal information and injuries data of any player within these
teams.er
"""
import requests
from bs4 import BeautifulSoup
import json
import config
from Soccerway_create_db import define_logger

TOP_LEAGUES_NUM = config.TOP_LEAGUES_NUM
SOCCER_URL = config.SOCCER_URL
LOGGER = config.LOGGER
LOGGER_FILE = config.LOG_FILE

logger = define_logger(LOGGER, LOGGER_FILE)

##########################################################


def get_top_leagues():

    comp_url = SOCCER_URL + "/competitions/"

    competitions = requests.get(comp_url).text

    competitions_soup = BeautifulSoup(competitions, 'html.parser')
    top_competitions = []

    # Run over competitions
    for competition in competitions_soup.find_all('li', class_=["odd", "even"]):
        top_competitions.append((competition.string, SOCCER_URL + competition.a['href']))

    top_competitions = top_competitions[:TOP_LEAGUES_NUM]
    logger.info(f"Links for the top {TOP_LEAGUES_NUM} leagues:\n")
    logger.info(str(top_competitions))

    save_json_competitions(top_competitions)

    return top_competitions

##########################################################


def get_teams_of_league(top_competitions):

    country_teams = {}

    for i in range(TOP_LEAGUES_NUM):

        country = top_competitions[i][1].rsplit('national/')[1].split('/')[0].capitalize()
        country_teams[country] = []

        specific_comp = requests.get(top_competitions[i][1]).text
        specific_comp_soup = BeautifulSoup(specific_comp, 'html.parser')

        league_table = specific_comp_soup.find('table', class_="leaguetable sortable table detailed-table")
        teams = league_table.find_all('td', class_="text team large-link")

        # Run over teams in a competition
        for team in teams:
            country_teams[country].append((team.a["title"], SOCCER_URL + team.a["href"]))

        logger.info(f"In the league of {country} there are {len(country_teams[country])}")

    save_json_teams(country_teams)

    return country_teams

##########################################################


def get_all_players(country_teams):

    team_players = {}

    # Run over players in each team of each country
    for key, value in country_teams.items():
        logger.info(f"started collecting info on players from {key}")
        team_players[key] = {}
        for team_name, team_url in value:

            # team_name = one_team.rsplit(key+'/')[1].split('/')[0]
            team_players[key][team_name] = {}

            squad_url = team_url + 'squad/'
            squad = requests.get(squad_url).text
            squad_soup = BeautifulSoup(squad, 'html.parser')

            squad_table = squad_soup.find('table', class_="table squad sortable")
            players = squad_table.find_all('td', class_="name large-link")

            # Scrape players data
            for player in players:
                team_players[key][team_name][player.string] = {}

                team_players[key][team_name][player.string]['url'] = SOCCER_URL + player.a["href"]
                # player_url = team_players[key][team_name][player.string]['url']

                player_content = requests.get(team_players[key][team_name][player.string]['url']).text
                player_soup = BeautifulSoup(player_content, 'html.parser')

                # Injuries
                sideline_table = player_soup.find('table', class_="sidelined table")
                team_players[key][team_name][player.string]['Injuries'] = {}

                if sideline_table:
                    injuries = sideline_table.find_all('tr', class_="odd")

                    injury_num = 1

                    # Looping over injuries of each player
                    for injury in injuries:
                        # if injury.td["title"] != 'Suspended':
                        team_players[key][team_name][player.string]['Injuries'][injury_num] = {}
                        team_players[key][team_name][player.string]['Injuries'][injury_num]['Description'] = \
                            injury.td["title"]

                        team_players[key][team_name][player.string]['Injuries'][injury_num]['Start'] = \
                            injury.find('td', class_="startdate").string

                        team_players[key][team_name][player.string]['Injuries'][injury_num]['End'] = \
                            injury.find('td', class_="enddate").string

                        injury_num += 1

                # Personal information
                passport = player_soup.find('div', class_="block_player_passport real-content clearfix")
                team_players[key][team_name][player.string]['Info'] = {}

                if passport:
                    details = passport.find_all('dt')
                    results = passport.find_all('dd')

                    # Looping over player's personal details
                    for i in range(len(details)):
                        team_players[key][team_name][player.string]['Info'][details[i].text] = results[i].text
            logger.info(f"collected info on players from {team_name}")
        logger.info(f"finished collecting info on players from the {key}")
    logger.info(f"finished collecting info on all players from the top {TOP_LEAGUES_NUM} leagues:\n")

    save_json_players(team_players)

    return team_players

##########################################################


# Save data as a JSON file
def save_json_players(team_players):
    with open("Team_Players.json", 'w') as json_file:
        json_file.write(json.dumps(team_players))


def save_json_competitions(top_competitions):
    with open("Top_Competitions.json", 'w') as json_file:
        json_file.write(json.dumps(top_competitions))


def save_json_teams(country_teams):
    with open("Country_Teams.json", 'w') as json_file:
        json_file.write(json.dumps(country_teams))

##########################################################


def main():
    top_competitions = get_top_leagues()

    country_teams = get_teams_of_league(top_competitions)

    get_all_players(country_teams)


if __name__ == '__main__':
    main()
