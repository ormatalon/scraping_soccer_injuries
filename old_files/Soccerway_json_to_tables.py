
### imports and constants

import pandas as pd
import argparse

#PLAYER_PATH = r'C:\Users\user\Desktop\ITC_bootcamp\scraping_soccer_injuries\Team_Players.json'


### Functions

# We create a unique id for each player, team and league that will later use us to find information from other tables
def extract_league_info(input_injuries_table):
    """
    creates a pd.dataframe with top leagues information
    :param input_injuries_table: pd.dataframe containing the data
    :return: pd.dataframe with top leagues information
    """
    leagues_info = pd.DataFrame()
    leagues_info['name'] = input_injuries_table.columns
    leagues_info['id'] = list(leagues_info.index)
    return leagues_info


def extract_team_info(input_injuries_table):
    """
    creates a pd.dataframe with all teams information
    :param input_injuries_table: pd.dataframe containing the data
    :return: pd.dataframe with all teams information
    """
    teams_info = pd.DataFrame()
    teams_info['name'] = input_injuries_table.index
    teams_info['id'] = list(teams_info.index)
    return teams_info


def extract_player_info(input_injuries_table):
    """
    create a pd.dataframe with all players information.
    :param input_injuries_table: pd.dataframe containing the data
    :return: pd.dataframe with all players information
    """
    player_info = pd.DataFrame()
    for league_ind,league in enumerate(input_injuries_table.keys()):
        teams_per_league = input_injuries_table[league].keys()[input_injuries_table[league].notnull()]
        print("reading {} teams".format(league))
        for team_ind, team in enumerate(teams_per_league):
            players_in_team = input_injuries_table[league][team]
            for player_ind, player in enumerate(players_in_team):
                players_in_team[player]['url']
                if (player_ind == 0 and team_ind == 0 and league_ind == 0):
                    player_info = pd.DataFrame(pd.Series(players_in_team[player]['Info'])).T
                else:
                    player_info = player_info.append(pd.Series(players_in_team[player]['Info']),ignore_index=True)
    return player_info


    player_info['id'] = list(player_info.index)
    return player_info


def extract_injuries_info(input_injuries_table):
    """
    extract organize and return all the injury instances in the database
    :param input_injuries_table: pd.dataframe containing the data
    :return: pd.dataframe containing all injury instances
    """
    columns_list = list(pd.DataFrame.from_dict(input_injuries_table["England"]["Liverpool"]).iloc[1,0]['1'].keys())
    columns_list.extend(["player","team","league"])
    injuries_table = pd.DataFrame(columns = columns_list)
    team_id = 0
    player_id = 0
    for league_ind, league in enumerate(input_injuries_table.keys()):
        teams_per_league = input_injuries_table[league].keys()[input_injuries_table[league].notnull()]
        print("reading {} teams".format(league))
        for team in teams_per_league:
            team_df = pd.DataFrame.from_dict(input_injuries_table[league][team])
            for player_ind in range(team_df.shape[1]):
                player_injuries = team_df.iloc[1,player_ind]
                injuries_table = injuries_table.append(pd.DataFrame(data=player_injuries).T)
                injuries_table.player = injuries_table.player.fillna(player_id)
                player_id += 1
            injuries_table.team = injuries_table.team.fillna(team_id)
            team_id += 1
        injuries_table.league = injuries_table.league.fillna(league_ind)
    return injuries_table

def main():
    """
    main function that runs all other functions if called
    :return:
    """
    input_injuries_table = pd.read_json(PLAYER_PATH)

    # craeting a csv file containing all chosen leagues
    leagues = extract_league_info(input_injuries_table)
    print("We used {0} leagues in out database".format(leagues.shape[0]))
    print(leagues)
    # craeting a csv file containing all teams
    teams = extract_team_info(input_injuries_table)
    print("The database contains {0} teams.".format(teams.shape[0]))
    # craeting a csv file containing all players
    players = extract_player_info(input_injuries_table)
    print("The database contains {0} players.".format(players.shape[0]))
    # Note not all players have absence reported thus are not found in the injuries table.
    # the injuries table contain data regarding only 1927 unique players while the players list contain 2909.
    injuries = extract_injuries_info(input_injuries_table)
    print("The database contains {0} absences.".format(injuries.shape[0]))


def get_args():
    """
    getting arguments from the command line
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("path", type=str, help="The path to Team_players.json file")
    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    PLAYER_PATH = args.path
    main()
