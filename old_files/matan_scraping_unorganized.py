import requests
from bs4 import BeautifulSoup
import json
import pandas as pd

TOP_LEAGUES_NUM = 5

soccer_url = "https://int.soccerway.com"
comp_url = soccer_url + "/competitions/"

competitions = requests.get(comp_url).text

competitions_soup = BeautifulSoup(competitions, 'html.parser')
top_competitions = []

# Run over competitions
for competition in competitions_soup.find_all('li', class_=["odd", "even"]):
    top_competitions.append((competition.string, soccer_url + competition.a['href']))

top_competitions = top_competitions[:TOP_LEAGUES_NUM]

print(f"Links for the top {TOP_LEAGUES_NUM} leagues:\n")
print(top_competitions, '\n')
country_teams = {}

##########################################################

for i in range(TOP_LEAGUES_NUM):

    country = top_competitions[i][1].rsplit('national/')[1].split('/')[0].capitalize()
    country_teams[country] = []

    specific_comp = requests.get(top_competitions[i][1]).text
    specific_comp_soup = BeautifulSoup(specific_comp, 'html.parser')

    league_table = specific_comp_soup.find('table', class_="leaguetable sortable table detailed-table")
    teams = league_table.find_all('td', class_="text team large-link")

    # Run over teams in a competition
    for team in teams:
        country_teams[country].append((team.a["title"], soccer_url + team.a["href"]))

    print(f"Links to each team in {country}:\n")
    print(country_teams[country], '\n')

##########################################################

team_players = {}

# Run over players in each team of each country
for key, value in country_teams.items():

    team_players[key] = {}
    for team_name, team_url in value:

        # team_name = one_team.rsplit(key+'/')[1].split('/')[0]
        team_players[key][team_name] = {}

        squad_url = team_url + 'squad/'
        squad = requests.get(squad_url).text
        squad_soup = BeautifulSoup(squad, 'html.parser')

        squad_table = squad_soup.find('table', class_="table squad sortable")
        players = squad_table.find_all('td', class_="name large-link")

        # Scrape players data
        for player in players:
            team_players[key][team_name][player.string] = {}

            team_players[key][team_name][player.string]['url'] = soccer_url + player.a["href"]
            # player_url = team_players[key][team_name][player.string]['url']

            player_content = requests.get(team_players[key][team_name][player.string]['url']).text
            player_soup = BeautifulSoup(player_content, 'html.parser')

            # Injuries
            sideline_table = player_soup.find('table', class_="sidelined table")
            team_players[key][team_name][player.string]['Injuries'] = {}

            if sideline_table:
                injuries = sideline_table.find_all('tr', class_="odd")

                injury_num = 1

                # Looping over injuries of each player
                for injury in injuries:
                    # if injury.td["title"] != 'Suspended':
                    team_players[key][team_name][player.string]['Injuries'][injury_num] = {}
                    team_players[key][team_name][player.string]['Injuries'][injury_num]['Description'] = \
                        injury.td["title"]

                    team_players[key][team_name][player.string]['Injuries'][injury_num]['Start'] = \
                        injury.find('td', class_="startdate").string

                    team_players[key][team_name][player.string]['Injuries'][injury_num]['End'] = \
                        injury.find('td', class_="enddate").string

                    injury_num += 1

            # Personal information
            passport = player_soup.find('div', class_="block_player_passport real-content clearfix")
            team_players[key][team_name][player.string]['Info'] = {}

            if passport:
                details = passport.find_all('dt')
                results = passport.find_all('dd')

                # Looping over player's personal details
                for i in range(len(details)):
                    team_players[key][team_name][player.string]['Info'][details[i].text] = results[i].text


print(f"Information of all players in top {TOP_LEAGUES_NUM} leagues:\n")
print(team_players)

##########################################################

# Save data as a JSON file
team_players_json = json.dumps(team_players)
with open("Team_Players.json", 'w') as json_file:
    json_file.write(team_players_json)

top_competitions_json = json.dumps(top_competitions)
with open("Top_Competitions.json", 'w') as json_file:
    json_file.write(top_competitions_json)

country_teams_json = json.dumps(country_teams)
with open("Country_Teams.json", 'w') as json_file:
    json_file.write(country_teams_json)
